export const environment = {
  production: true,
  apiUrl: 'https://site.com:443/api/',
  imgUrl: 'https://site.com:443/api/download/',
};
