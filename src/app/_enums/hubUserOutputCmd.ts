export enum HubUserOutputCmd {
  Pong = "Pong",
  CheckUnreadNotification = "CheckUnreadNotification"
}
