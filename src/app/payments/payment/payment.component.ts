import { Component, OnInit, OnDestroy } from "@angular/core";
import { AlertifyService } from "src/app/_services/alertify.service";
import { AuthService } from "src/app/_services/auth.service";
import { ProductsService } from "src/app/_services/products.service";
import { TariffPlanService } from "../_services/tariff-plan.service";
import { Subscription } from "rxjs";
import { User } from "src/app/_models/user";
import { TariffPlan } from "../_models/tariff-plan";
import { Product } from "src/app/_models/product";
import { ManTariffs } from "src/app/_enums/manTariffs";
import { LoaderService } from "src/app/_services/loader.service";
import { HeaderService } from "src/app/_services/header.service";

@Component({
  selector: "anna-mobile-payment",
  templateUrl: "./payment.component.html",
  styleUrls: ["./payment.component.css"]
})
export class PaymentComponent implements OnInit, OnDestroy {
  tariffPlans: TariffPlan[];
  baseTariffPlan: TariffPlan;
  tariffs: Product[];
  photo: Product;
  video: Product;
  mail: Product;
  chat: Product;

  currentUser: User;
  httpSubscription: Subscription;
  httpTariffsSubscription: Subscription;

  constructor(
    private tariffPlanService: TariffPlanService,
    private productService: ProductsService,
    private authService: AuthService,
    private alertify: AlertifyService,
    private loaderService: LoaderService,
    private headerService: HeaderService
  ) {}

  ngOnInit() {
    this.headerService.resetHeader();
    this.headerService.changeTitle("Purchase Credits");
    this.headerService.setShowHaveCredits(true);
    this.headerService.setShowActiveChats(false);

    this.loadTariffPlans();
    this.loadTariffs();
    this.currentUser = this.authService.getCurrentUser();
  }

  loadTariffPlans() {
    this.loaderService.setIsShowLoader(true);
    this.httpSubscription = this.tariffPlanService.getTariffPlans().subscribe(
      (res: TariffPlan[]) => {
        this.baseTariffPlan = res.find(x => x.isBasePlan);
        this.tariffPlans = res;
        this.loaderService.setIsShowLoader(false);
        this.httpSubscription.unsubscribe();
      },
      error => {
        this.loaderService.setIsShowLoader(false);
        this.httpSubscription.unsubscribe();
        this.alertify.error(error.error);
      }
    );
  }

  loadTariffs() {
    this.httpTariffsSubscription = this.productService.getTariffs().subscribe(
      (res: Product[]) => {
        this.tariffs = res;
        this.photo = this.tariffs.find(x => x.title === ManTariffs.ViewPhoto);
        this.video = this.tariffs.find(x => x.title === ManTariffs.WatchVideo);
        this.mail = this.tariffs.find(x => x.title === ManTariffs.SendingMail);
        this.chat = this.tariffs.find(x => x.title === ManTariffs.LiveChat);

        this.httpTariffsSubscription.unsubscribe();
      },
      error => {
        this.httpTariffsSubscription.unsubscribe();
        this.alertify.error(error.error);
      }
    );
  }

  showPayment(tariffPlan: TariffPlan) {
    window.location.href = tariffPlan.url;
  }

  onClickBuy(tariffPlan: TariffPlan) {
    this.showPayment(tariffPlan);
  }

  unsubscribe() {
    if (this.httpSubscription) this.httpSubscription.unsubscribe();
    if (this.httpTariffsSubscription)
      this.httpTariffsSubscription.unsubscribe();
  }

  ngOnDestroy(): void {
    this.unsubscribe();
    this.loaderService.setIsShowLoader(false);
  }
}
