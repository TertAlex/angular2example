import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { TariffPlan } from '../_models/tariff-plan';

@Injectable()
export class TariffPlanService {
  
  baseUrl = environment.apiUrl + 'tariffplans/';
  
  constructor(private httpClient: HttpClient) { }

  getTariffPlans() {
		return this.httpClient.get<TariffPlan[]>(this.baseUrl)
	}
}