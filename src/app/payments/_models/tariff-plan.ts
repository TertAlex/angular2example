export class TariffPlan {
  id: number;
  amountCredits: number;
  amountCurrency: number;
  currency: string;
  description: string;
  isOneTimeOffer: boolean;
  isBasePlan: boolean;
  url: string;
}