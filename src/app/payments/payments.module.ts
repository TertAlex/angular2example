import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import {
  PaymentsRoutedComponents,
  PaymentsRoutingModule
} from "./payments.routing";
import { TariffPlanService } from "./_services/tariff-plan.service";
import { FlexLayoutModule } from "@angular/flex-layout";

@NgModule({
  imports: [CommonModule, PaymentsRoutingModule, FlexLayoutModule],
  declarations: [PaymentsRoutedComponents],
  providers: [TariffPlanService]
})
export class PaymentsModule {}
