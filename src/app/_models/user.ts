export class User {
  id: number;
  username: string;
  password: string;
  knowAs: string;
  age: number;
  dateOfBirth: string;
  created: Date;
  lastPing: Date;
  photoId: number;
  isEmailConfirmed: boolean;
  mobilePhone: string;
  isAcceptedCalls: boolean;

  countryId: number;
  country: string;
  cityId: number;
  city: string;

  isOnline: boolean;

  public updateUser(user: User): void {
    this.country = user.country;
    this.city = user.city;
  }
}

export enum UserContainer {
  Online = "online"
}
