export class Pagination {
  currentPage: number;
  itemsPerPage: number;
  totalItems: number;
  totalPages: number;

  constructor() {
      this.currentPage = 1;
      this.itemsPerPage = 10;
      this.totalItems = 0;
      this.totalPages = 1;
  }
}

export class PaginatedResult<T> {
  result: T;
  pagination: Pagination;
}