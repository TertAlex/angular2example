import { Pagination } from './Pagination';

export class PagedResult<T>{
    totalCount: number;
    items: T[];
    pagination: Pagination;
}