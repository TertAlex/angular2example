import { Injectable } from "@angular/core";
import { BehaviorSubject, Subject } from "rxjs";

@Injectable()
export class HeaderService {
  private titleSubject = new BehaviorSubject<string>(null);
  public title$ = this.titleSubject.asObservable();
  
  private subTitleSubject = new BehaviorSubject<string>(null);
  public subTitle$ = this.subTitleSubject.asObservable();
  
  private clickTitleSubject: Subject<null> = new Subject();
  public clickTitle$ = this.clickTitleSubject.asObservable();

  private isShowBack: boolean = false;
  private isShowBackSubject = new BehaviorSubject<boolean>(false);
  public isShowBack$ = this.isShowBackSubject.asObservable();

  private photoIdSubject = new BehaviorSubject<number>(null);
  public photoId$ = this.photoIdSubject.asObservable();

  private isShowArrowDownSubject = new BehaviorSubject<boolean>(false);
  public isShowArrowDown$ = this.isShowArrowDownSubject.asObservable();

  private isShowArrowUpSubject = new BehaviorSubject<boolean>(false);
  public isShowArrowUp$ = this.isShowArrowUpSubject.asObservable();

  private chatRecipientNameSubject = new BehaviorSubject<string>(null);
  public chatRecipientName$ = this.chatRecipientNameSubject.asObservable();

  private isShowExtraButtonSubject = new BehaviorSubject<boolean>(false);
  public isShowExtraButton$ = this.isShowExtraButtonSubject.asObservable();

  private extraButtonTitleSubject = new BehaviorSubject<string>(null);
  public extraButtonTitle$ = this.extraButtonTitleSubject.asObservable();

  private clickExtraButtonSubject : Subject<null> = new Subject();
  public clickExtraButton$ = this.clickExtraButtonSubject.asObservable();

  private clickBackSubject : Subject<null> = new Subject();
  public clickBack$ = this.clickBackSubject.asObservable();

  private isShowHaveCreditsSubject = new BehaviorSubject<boolean>(false);
  public isShowHaveCredits$ = this.isShowHaveCreditsSubject.asObservable();

  private isShowActiveChatsSubject = new BehaviorSubject<boolean>(false);
  public isShowActiveChats$ = this.isShowActiveChatsSubject.asObservable();

  constructor() {}

  changeTitle(title: string) {
    this.titleSubject.next(title);
  }

  changeSubTitle(subTitle: string){
    this.subTitleSubject.next(subTitle);
  }

  setIsShowBack(isShow: boolean) {
    this.isShowBack = isShow;
    this.isShowBackSubject.next(this.isShowBack);
  }

  toggleIsShowBack() {
    if (this.isShowBack) {
      this.isShowBack = false;
    } else {
      this.isShowBack = true;
    }
    this.isShowBackSubject.next(this.isShowBack);
  }

  setIsShowArrowDown(isShow: boolean) {
    this.isShowArrowDownSubject.next(isShow);
  }

  setIsShowArrowUp(isShow: boolean) {
    this.isShowArrowUpSubject.next(isShow);
  }

  setPhotoId(photoId: number){
    this.photoIdSubject.next(photoId);
  }

  changeChatRecipientName(name: string){
    this.chatRecipientNameSubject.next(name);
  }

  setShowExtraButton(isShow: boolean, title: string){
    this.isShowExtraButtonSubject.next(isShow);
    this.extraButtonTitleSubject.next(title);
  }

  public resetHeader(){
    this.titleSubject.next(null);
    this.subTitleSubject.next(null);
    this.photoIdSubject.next(null);
    this.isShowArrowDownSubject.next(false);
    this.isShowArrowUpSubject.next(false);
    this.chatRecipientNameSubject.next(null);
    this.setIsShowBack(false);
    this.isShowExtraButtonSubject.next(false);
    this.extraButtonTitleSubject.next(null);
    this.isShowHaveCreditsSubject.next(false);
    this.isShowActiveChatsSubject.next(true);
  }

  public clickExtraButton(){
    this.clickExtraButtonSubject.next(null);
  }

  public clickBack(){
    this.clickBackSubject.next(null);
  }

  public clickTitle(){
    this.clickTitleSubject.next(null);
  }

  public setShowHaveCredits(isShow: boolean){
    this.isShowHaveCreditsSubject.next(isShow);
  }

  public setShowActiveChats(isShow: boolean){
    this.isShowActiveChatsSubject.next(isShow);
  }
} 