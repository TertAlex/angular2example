import { Injectable } from "@angular/core";
import { User } from "../_models/user";

@Injectable()
export class DataService {

  private user: User;

  constructor() {}

  public setUser(user: User){
    this.user = user;
  }

  public getUser(){
    return this.user;
  }
}
