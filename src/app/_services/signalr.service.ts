import { environment } from "src/environments/environment";
import { Injectable } from "@angular/core";
import { BehaviorSubject, Subscription } from "rxjs";
import { HubConnection } from "@aspnet/signalr";
import * as signalR from "@aspnet/signalr";
import { User } from "../_models/user";
import { AuthService } from "./auth.service";
import { signalRConfig } from "src/environments/signalR";
import { HubUserOutputCmd } from "../_enums/hubUserOutputCmd";
import { HubUserInputCmd } from "../_enums/hubUserInputCmd";

@Injectable({ providedIn: "root" })
export class SignalRService {
  private _messagesHubConnection: HubConnection;
  isConnect$: BehaviorSubject<boolean>;
  private currentUser: User;
  userToken: any;
  isConnect: boolean = false;
  reconnectTimerId: any;
  allowReconnect: boolean = true;

  private logined: Subscription;

  constructor(private authService: AuthService) {
    this.isConnect$ = new BehaviorSubject<boolean>(false);
    this.currentUser = this.authService.getCurrentUser();
    this.userToken = this.authService.getUserToken();
  }

  ngOnInit(): void {
    this.logined = this.authService.logined.subscribe(isLogined => {
      if (!isLogined) {
        this.closeConnection();
      }
    });
  }

  public createConnection() {
    this.openConnection();
  }

  public closeConnection() {
    // this.isConnect$ = null;
    this.isConnect$.next(false);
    if (this._messagesHubConnection) this._messagesHubConnection.stop();
  }

  private openConnection() {
    if (!this.currentUser || !this.currentUser.id) return;

    if (this.authService.isTokenExpired()) return;

    this._messagesHubConnection = new signalR.HubConnectionBuilder()
      .withUrl(environment.apiUrl + signalRConfig.messages_url)
      .build();

    this.start();

    this._messagesHubConnection.onclose(() => {
      this.isConnect$.next(false);
      this.isConnect = false;
      this.reconnectTimerId = setInterval(() => {
        if (this.isConnect) {
          if (this.reconnectTimerId) clearInterval(this.reconnectTimerId);
        } else {
          if (this.authService.loggedIn()) {
            if (!this.allowReconnect) {
              clearInterval(this.reconnectTimerId);
            }
            this.start();
          } else {
            clearInterval(this.reconnectTimerId);
          }
        }
      }, 2000);
    });
  }

  private start() {
    this._messagesHubConnection
      .start()
      .then(() => {
        this.isConnect = true;
        clearInterval(this.reconnectTimerId);
      })
      .catch(err => console.error("Error while establishing connection :("));
  }

  public getConnection() {
    return this._messagesHubConnection;
  }

  subscriberToPing() {
    this._messagesHubConnection.on(HubUserInputCmd.Ping, resp => {
      this._messagesHubConnection.invoke(HubUserOutputCmd.Pong);
    });
  }

  ngOnDestroy() {
    this.isConnect$.next(false);
    if (this.logined) this.logined.unsubscribe();
    this._messagesHubConnection.off(HubUserInputCmd.Ping);
    this._messagesHubConnection.stop();
    clearInterval(this.reconnectTimerId);
  }
}
