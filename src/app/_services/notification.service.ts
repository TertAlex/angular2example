import { AuthService } from "./auth.service";
import { SignalRService } from "./signalr.service";
import { Subject } from "rxjs";
import { HubConnection } from "@aspnet/signalr";
import { Injectable, OnInit, OnDestroy } from "@angular/core";
import { HubUserOutputCmd } from "../_enums/hubUserOutputCmd";
import { HubUserInputCmd } from "../_enums/hubUserInputCmd";

@Injectable()
export class NotificationService implements OnInit, OnDestroy {
  ngOnInit(): void {}
  private hubConnection: HubConnection;

  countOnlineUser$ = new Subject<number>();


  constructor(
    private signalRService: SignalRService,
    private authService: AuthService
  ) {
    signalRService.isConnect$.subscribe(value => {
      if (value == true) {
        this.hubConnection = this.signalRService.getConnection();
        this.subscribe();
        this.checkUnreadNotification();
      }
    });
  }

  checkUnreadNotification() {
    return this.hubConnection.invoke(HubUserOutputCmd.CheckUnreadNotification);
  }

  subscribe() {
    this.hubConnection.on(HubUserInputCmd.CountOnlineUser, resp =>
      this.countOnlineUser$.next(resp)
    );
  }

  ngOnDestroy(): void {
    this.hubConnection.off(HubUserInputCmd.CountOnlineUser);
  }
}
