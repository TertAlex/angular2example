import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable()
export class MenuService {
  private isShowMenu: boolean = false;
  private isShowMenuSubject = new BehaviorSubject<boolean>(false);
  public isShowMenu$ = this.isShowMenuSubject.asObservable();

  constructor() {}

  setIsShowMenu(isShow: boolean) {
    this.isShowMenu = isShow;
    this.isShowMenuSubject.next(this.isShowMenu);
  }

  toggleIsShowMenu() {
    if (this.isShowMenu) {
      this.setIsShowMenu(false);
    } else {
      this.setIsShowMenu(true);
    }
  }
}
