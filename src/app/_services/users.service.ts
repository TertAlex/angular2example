import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { User } from '../_models/user';
import { map } from 'rxjs/operators';
import { PaginatedResult } from '../_models/pagination';
import { Observable } from 'rxjs';
import { UserParams } from '../_sctructures/user.params';

@Injectable({providedIn: 'root'})
export class UsersService {

  baseUrl = environment.apiUrl;
  
  constructor(private httpClient: HttpClient) { }

  getUser(id): Observable<User> {
    return this.httpClient.get<User>(this.baseUrl + 'users/' + id);
  }

  getUsers(userParams: UserParams) {
    const paginatedResult: PaginatedResult<User[]> = new PaginatedResult<User[]>();
    let params = new HttpParams();

    params = userParams.addUserParams(params);

    return this.httpClient.get<User[]>(this.baseUrl + 'users', { observe: 'response', params }).pipe( //we need to specify  observe: 'response' in order to get Response back instead of just a body
      map(response => {
        paginatedResult.result = response.body; //getting result from body
        if (response.headers.get('Pagination') != null) { //checking if we have some data regarding current page itemsPerPage, totalItems and totalPages. 
          paginatedResult.pagination = JSON.parse(response.headers.get('Pagination'));
        }

        return paginatedResult;
      }))
  }

  updateUser(id: number, user: User) {
    return this.httpClient.put(this.baseUrl + 'users/' + id, user);
  }
}