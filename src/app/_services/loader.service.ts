import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";

@Injectable()
export class LoaderService {
  private isShowLoader: boolean = false;
  private isShowLoaderSubject = new BehaviorSubject<boolean>(false);
  public isShowLoader$ = this.isShowLoaderSubject.asObservable();

  constructor() {}

  setIsShowLoader(isShow: boolean) {
    this.isShowLoader = isShow;
    this.isShowLoaderSubject.next(isShow);
  }
}
