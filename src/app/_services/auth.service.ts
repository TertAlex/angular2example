import { Injectable } from "@angular/core";
import { environment } from "src/environments/environment";
import { JwtHelperService } from "@auth0/angular-jwt";
import { AlertifyService } from "./alertify.service";
import { Router } from "@angular/router";
import { BehaviorSubject } from "rxjs";
import { map } from "rxjs/operators";
import { User } from "../_models/user";
import { HttpHeaders, HttpClient } from "@angular/common/http";
import { AuthUser } from "../_models/authUser";

@Injectable({
  providedIn: "root"
})
export class AuthService {
  baseUrl = environment.apiUrl + "auth/";
  userToken: any;
  private decodedToken: any;
  private currentUser: User;

  logined = new BehaviorSubject<boolean>(false);
  private userSubject = new BehaviorSubject<User>(this.currentUser);
  private photoId = new BehaviorSubject<number>(0);

  currentPhotoId = this.photoId.asObservable();
  userUpdated = this.userSubject.asObservable();

  constructor(
    private jwtHelperService: JwtHelperService,
    private alertify: AlertifyService,
    private router: Router,
    private httpClient: HttpClient
  ) {}

  loggedIn() {
    const token = this.jwtHelperService.tokenGetter();
    if (!token) {
      return false;
    }
    return !this.jwtHelperService.isTokenExpired(token);
  }

  logout() {
    this.userToken = null;
    this.updateUser(null);
    localStorage.removeItem("token");
    localStorage.removeItem("user");
    this.alertify.message("logged out!");

    this.logined.next(false);
    this.router.navigate(["/auth/login"]);
  }

  updateUser(user: User) {
    this.currentUser = user;
    this.updateLocalUser();
    this.userSubject.next(user);
  }

  updateLocalUser() {
    localStorage.setItem("user", JSON.stringify(this.currentUser));
  }

  updateUserCredits(credits: number) {
    this.currentUser.credits = credits;
    this.updateLocalUser();
  }

  public getCurrentUser() {
    this.loadUserFromLocalStorage();
    return this.currentUser;
  }

  loadUserFromLocalStorage() {
    if (!this.currentUser) {
      this.currentUser = JSON.parse(localStorage.getItem("user"));
      if (this.currentUser) this.changeMemberPhoto(this.currentUser.photoId);
      this.userSubject.next(this.currentUser);
    }
  }

  register(model: any) {
    return this.httpClient.post(this.baseUrl + "register", model, {
      headers: new HttpHeaders().set("Content-Type", "application/json")
    });
  }

  login(model: any) {
    return this.httpClient
      .post<AuthUser>(this.baseUrl + "login", model, {
        headers: new HttpHeaders() //Specify <AuthUser> in order to help map(user) see properties
          .set("Content-Type", "application/json")
      })
      .pipe(
        map(user => {
          if (user && user.tokenString) {
            localStorage.setItem("token", user.tokenString);
            localStorage.setItem("user", JSON.stringify(user.user));
            this.decodedToken = this.jwtHelperService.decodeToken(
              user.tokenString
            );
            this.currentUser = user.user;
            this.userToken = user.tokenString;
            this.userSubject.next(this.currentUser);

            if (
              this.currentUser.photoId !== null ||
              this.currentUser.photoId !== undefined
            ) {
              this.changeMemberPhoto(this.currentUser.photoId);
            } else {
              this.changeMemberPhoto(0); //do not foget to add it globaly in app.component.ts. It will help to avoid issue with refresh button
            }

            this.logined.next(true);
          }
        })
      );
  }

  changeMemberPhoto(photoId: number) {
    this.photoId.next(photoId); //assigning value for photoUrl BehaviorSubject
    this.currentUser.photoId = photoId;
    localStorage.setItem("user", JSON.stringify(this.currentUser));
  }

  public getUserToken() {
    if (!this.userToken) this.userToken = localStorage.getItem("token");
    return this.userToken;
  }

  public isUserMale() {
    this.loadUserFromLocalStorage();
    return this.currentUser.gender === "male";
  }

  loadTokenFromLocalStorage() {
    this.userToken = localStorage.getItem("token");
    this.decodedToken = this.jwtHelperService.decodeToken(this.userToken);
  }

  isTokenExpired() {
    this.loadTokenFromLocalStorage();
    return this.jwtHelperService.isTokenExpired(this.userToken);
  }
}
