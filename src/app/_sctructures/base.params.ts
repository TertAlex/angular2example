import { HttpParams } from "@angular/common/http";

export class BaseParams {
  pageNumber: number;
  pageSize: number;
  sortActive: string;
  sortDirection: string;
  dateFrom: Date;
  dateTo: Date;

  public AddBaseParams(params: HttpParams): HttpParams {
    if (this.pageNumber) {
      params = params.append("pageNumber", String(this.pageNumber));
    }
    if (this.pageSize) {
      params = params.append("pageSize", String(this.pageSize));
    }
    if (this.sortActive) {
      params = params.append("sortActive", this.sortActive);
    }
    if (this.sortDirection) {
      params = params.append("sortDirection", this.sortDirection);
    }
    if (this.dateFrom) {
      params = params.append("dateFrom", this.dateFrom.toJSON());
    }
    if (this.dateTo) {
      params = params.append("dateTo", this.dateTo.toJSON());
    }
    return params;
  }
}
