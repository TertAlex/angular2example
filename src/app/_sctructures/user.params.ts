import { HttpParams } from "@angular/common/http";
import { BaseParams } from "./base.params";
import { UserContainer } from "../_models/user";

export class UserParams extends BaseParams {
  isOnline: boolean;
  country: string;
  excludedUserIds: number[];
  container: UserContainer;

  constructor() {
    super();
    this.isOnline = false;
  }

  public addUserParams(params: HttpParams): HttpParams {
    params = this.AddBaseParams(params);

    if (this.isOnline) {
      params = params.append("isOnline", String(this.isOnline));
    }
    if (this.country) {
      params = params.append("country", this.country);
    }
    if (this.excludedUserIds) {
      params = params.append("excludedUserIds", this.excludedUserIds.join(","));
    }
    if (this.container) {
      params = params.append("container", this.container);
    }

    return params;
  }
}
