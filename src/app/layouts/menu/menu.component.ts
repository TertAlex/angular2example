import { Component, OnInit, OnDestroy } from "@angular/core";
import { AuthService } from "src/app/_services/auth.service";
import { Router } from "@angular/router";
import { MenuService } from "src/app/_services/menu.service";
import { Subject } from "rxjs";
import { takeUntil } from "rxjs/operators";
import { UserContainer, User } from "src/app/_models/user";
import { environment } from "src/environments/environment";
import { NotificationService } from "src/app/_services/notification.service";

@Component({
  selector: "angular2-example-menu",
  templateUrl: "./menu.component.html",
  styleUrls: ["./menu.component.css"]
})
export class MenuComponent implements OnInit, OnDestroy {
  imageApi: string = environment.imgUrl + "/";

  countOnlineUser: number;
  currentUser: User;

  private destroy$: Subject<boolean> = new Subject<boolean>();

  constructor(
    private authService: AuthService,
    private router: Router,
    private menuService: MenuService,
    private notificationService: NotificationService
  ) {}

  ngOnInit() {
    this.currentUser = this.authService.getCurrentUser();

    this.subscribeToNotification();
  }

  onClickProfile() {
    this.menuService.setIsShowMenu(false);
    this.router.navigate(["members/profile"]);
  }

  onClickOnline() {
    this.menuService.setIsShowMenu(false);
    this.router.navigate(["members"], {
      queryParams: { container: UserContainer.Online }
    });
  }

  onClickPurchases() {
    this.menuService.setIsShowMenu(false);
    this.router.navigate(["payment"]);
  }

  onClickLogout() {
    this.authService.logout();
  }

  onClickSwitchToDesktop() {
    window.location.href = "http://www.site.com";
  }

  subscribeToNotification() {
    this.notificationService.countOnlineUser$
      .pipe(takeUntil(this.destroy$))
      .subscribe(countOnlineUser => {
        if (countOnlineUser > 0) {
          this.countOnlineUser = countOnlineUser;
        } else {
          this.countOnlineUser = undefined;
        }
      });
  }

  ngOnDestroy() {
    this.destroy$.next(true);
    this.destroy$.unsubscribe();
  }
}
