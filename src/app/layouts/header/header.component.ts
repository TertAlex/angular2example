import { Component, OnInit, OnDestroy } from "@angular/core";
import { MenuService } from "src/app/_services/menu.service";
import { HeaderService } from "src/app/_services/header.service";
import { Subscription } from "rxjs";
import { environment } from "src/environments/environment";
import { User } from "src/app/_models/user";
import { AuthService } from "src/app/_services/auth.service";

@Component({
  selector: "angular2-example-header",
  templateUrl: "./header.component.html",
  styleUrls: ["./header.component.css"]
})
export class HeaderComponent implements OnInit, OnDestroy {
  imageApi: string = environment.imgUrl + "/";

  title: string;
  titleChangeSubscription: Subscription;

  subTitle: string;
  subTitleChangeSubcription: Subscription;

  isShowBack: boolean = false;
  isShowBackSubscription: Subscription;

  photoId: number;
  photoIdChangeSubscription: Subscription;

  isShowArrowDown: boolean = false;
  isShowArrowDownSubscription: Subscription;

  isShowArrowUp: boolean = false;
  isShowArrowUpSubscription: Subscription;

  chatRecipientName: string;
  chatRecipientNameSubscription: Subscription;

  isShowExtraButton: boolean = false;
  isShowExtraButtonSubscription: Subscription;

  extraButtonTitle: string;
  extraButtonTitleSubscription: Subscription;

  isShowHaveCredits: boolean = false;
  isShowHaveCreditsSubscription: Subscription;

  isShowActiveChats: boolean = false;
  isShowActiveChatsSubscription: Subscription;

  currentUser: User;

  constructor(
    private menuService: MenuService,
    private headerService: HeaderService,
    private authService: AuthService
  ) {}

  ngOnInit() {
    this.sibscribeTitleChanged();
    this.currentUser = this.authService.getCurrentUser();
  }

  onClickMenu() {
    this.menuService.toggleIsShowMenu();
  }

  sibscribeTitleChanged() {
    this.titleChangeSubscription = this.headerService.title$.subscribe(
      title => {
        this.title = title;
      }
    );

    this.subTitleChangeSubcription = this.headerService.subTitle$.subscribe(
      subTitle => {
        this.subTitle = subTitle;
      }
    );

    this.isShowBackSubscription = this.headerService.isShowBack$.subscribe(
      isShowBack => {
        this.isShowBack = isShowBack;
      }
    );

    this.photoIdChangeSubscription = this.headerService.photoId$.subscribe(
      photoId => {
        this.photoId = photoId;
      }
    );

    this.isShowArrowDownSubscription = this.headerService.isShowArrowDown$.subscribe(
      isShow => {
        this.isShowArrowDown = isShow;
      }
    );

    this.isShowArrowUpSubscription = this.headerService.isShowArrowUp$.subscribe(
      isShow => {
        this.isShowArrowUp = isShow;
      }
    );

    this.chatRecipientNameSubscription = this.headerService.chatRecipientName$.subscribe(
      name => {
        this.chatRecipientName = name;
      }
    );

    this.isShowExtraButtonSubscription = this.headerService.isShowExtraButton$.subscribe(
      isShow => {
        this.isShowExtraButton = isShow;
      }
    );

    this.extraButtonTitleSubscription = this.headerService.extraButtonTitle$.subscribe(
      title => {
        this.extraButtonTitle = title;
      }
    );

    this.isShowHaveCreditsSubscription = this.headerService.isShowHaveCredits$.subscribe(
      isShow => {
        this.isShowHaveCredits = isShow;
      }
    );

    this.isShowActiveChatsSubscription = this.headerService.isShowActiveChats$.subscribe(
      isShow => {
        this.isShowActiveChats = isShow;
      }
    );
  }

  onClickBack() {
    this.headerService.clickBack();
    // this.location.back();
  }

  onClickActiveChats() {
  }

  ngOnDestroy(): void {
    if (this.titleChangeSubscription)
      this.titleChangeSubscription.unsubscribe();
    if (this.isShowBackSubscription) this.isShowBackSubscription.unsubscribe();
    if (this.photoIdChangeSubscription)
      this.photoIdChangeSubscription.unsubscribe();
    if (this.chatRecipientNameSubscription)
      this.chatRecipientNameSubscription.unsubscribe();
    if (this.isShowExtraButtonSubscription)
      this.isShowExtraButtonSubscription.unsubscribe();
    if (this.extraButtonTitleSubscription)
      this.extraButtonTitleSubscription.unsubscribe();
    if (this.isShowHaveCreditsSubscription)
      this.isShowHaveCreditsSubscription.unsubscribe();
    if (this.isShowActiveChatsSubscription)
      this.isShowActiveChatsSubscription.unsubscribe();
    if (this.subTitleChangeSubcription)
      this.subTitleChangeSubcription.unsubscribe();
    if (this.isShowArrowDownSubscription)
      this.isShowArrowDownSubscription.unsubscribe();
    if (this.isShowArrowUpSubscription)
      this.isShowArrowUpSubscription.unsubscribe();
  }

  onClickExtraButton() {
    this.headerService.clickExtraButton();
  }

  onTitleClick() {
    this.headerService.clickTitle();
  }
}
