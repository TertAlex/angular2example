import { NgModule } from "@angular/core";

import { UserLayoutComponent } from "./user-layout/user-layout.component";
import { MenuComponent } from "./menu/menu.component";
import { HeaderComponent } from "./header/header.component";
import { RouterModule } from "@angular/router";
import { FlexLayoutModule } from "@angular/flex-layout";
import { CommonModule } from "@angular/common";
import { InfiniteScrollModule } from "ngx-infinite-scroll";
import { FooterComponent } from "./footer/footer.component";

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FlexLayoutModule,
    InfiniteScrollModule,
  ],
  exports: [UserLayoutComponent, MenuComponent, HeaderComponent],
  declarations: [
    UserLayoutComponent,
    MenuComponent,
    HeaderComponent,
    FooterComponent
  ],
  providers: []
})
export class LayoutsModule {}
