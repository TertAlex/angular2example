import { Component, OnInit, OnDestroy, AfterViewInit } from "@angular/core";
import { MenuService } from "src/app/_services/menu.service";
import { Subscription } from "rxjs";
import { LoaderService } from "src/app/_services/loader.service";

@Component({
  selector: "angular2-example-user-layout",
  templateUrl: "./user-layout.component.html",
  styleUrls: ["./user-layout.component.css"]
})
export class UserLayoutComponent implements OnInit, OnDestroy, AfterViewInit {
  isShowMenu: boolean = true;
  changeMenuSubscription: Subscription;

  isShowLoader: boolean;
  loaderSubscription: Subscription;

  constructor(
    private menuService: MenuService,
    private loaderService: LoaderService
  ) {}

  ngOnInit() {
    this.subscribeChangeMenu();
  }

  ngAfterViewInit(): void {}

  subscribeChangeMenu() {
    this.changeMenuSubscription = this.menuService.isShowMenu$.subscribe(
      (isShow: boolean) => {
        this.isShowMenu = isShow;
      }
    );

    this.loaderSubscription = this.loaderService.isShowLoader$.subscribe(
      (isShow: boolean) => {
        this.isShowLoader = isShow;
      }
    );
  }

  ngOnDestroy(): void {
    if (this.changeMenuSubscription) this.changeMenuSubscription.unsubscribe();
    if (this.loaderSubscription) this.loaderSubscription.unsubscribe();
  }
}
