import { Routes, RouterModule } from "@angular/router";
import { NgModule } from "@angular/core";
import { AuthGuard } from "./_guards/auth.guard";
import { UserLayoutComponent } from "./layouts/user-layout/user-layout.component";

const routes: Routes = [
  {
    path: "auth",
    loadChildren: "./auth/auth.module#AuthModule"
  },
  {
    path: "",
    runGuardsAndResolvers: "always",
    component: UserLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: "",
        redirectTo: "members", pathMatch: "full"
      },
      {
        path: "members",
        loadChildren: "./members/members.module#MembersModule"
      },
      {
        path: "payment",
        loadChildren: "./payments/payments.module#PaymentsModule"
      }
    ]
  },
  { path: "**", redirectTo: "", pathMatch: "full" }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}

export const AppRoutedComponents = [];
